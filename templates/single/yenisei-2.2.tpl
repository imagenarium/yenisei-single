<@requirement.NODE 'yns' 'yns-${namespace}' />

<@requirement.PARAM name='YENISEI_USER' value='admin' scope='global' />
<@requirement.PARAM name='YENISEI_PASSWORD' scope='global' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />
<@requirement.PARAM name='PUBLISHED_PORT_TYPE' values='global,local' value='global' type='select' depends='PUBLISHED_PORT' />
<@requirement.PARAM name='RECV_BUFFER' value='262144' />
<@requirement.PARAM name='PARTITIONS' value='8' />

<@img.TASK 'yns-${namespace}' 'imagenarium/yenisei:2.2'>
  <@img.NODE_REF 'yns' />
  <@img.PORT PARAMS.PUBLISHED_PORT '5984' PARAMS.PUBLISHED_PORT_TYPE />

  <@img.VOLUME '/opt/yenisei/data' />
  <@img.VOLUME '/etc/yenisei' />

  <@img.ENV 'RECV_BUFFER' PARAMS.RECV_BUFFER />
  <@img.ENV 'YENISEI_USER' PARAMS.YENISEI_USER />
  <@img.ENV 'YENISEI_PASSWORD' PARAMS.YENISEI_PASSWORD />
  <@img.ENV 'PARTITIONS' PARAMS.PARTITIONS />
  <@img.ENV 'YENISEI_COOKIE' statics["java.util.UUID"].randomUUID() />

  <@img.CHECK_PORT '5984' />
</@img.TASK>
